
class Helpers {
  constructor() {

  }

  public static convertDateToUTC(date) {
    return new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds());
  }

  public static deepCompare () {
    var i, l, leftChain, rightChain;

    function compare2Objects (x, y) {
      var p;

      // remember that NaN === NaN returns false
      // and isNaN(undefined) returns true
      if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
           return true;
      }

      // Compare primitives and functions.
      // Check if both arguments link to the same object.
      // Especially useful on the step where we compare prototypes
      if (x === y) {
          return true;
      }

      // Works in case when functions are created in constructor.
      // Comparing dates is a common scenario. Another built-ins?
      // We can even handle functions passed across iframes
      if ((typeof x === 'function' && typeof y === 'function') ||
         (x instanceof Date && y instanceof Date) ||
         (x instanceof RegExp && y instanceof RegExp) ||
         (x instanceof String && y instanceof String) ||
         (x instanceof Number && y instanceof Number)) {
          return x.toString() === y.toString();
      }

      // At last checking prototypes as good as we can
      if (!(x instanceof Object && y instanceof Object)) {
          return false;
      }

      if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
          return false;
      }

      if (x.constructor !== y.constructor) {
          return false;
      }

      if (x.prototype !== y.prototype) {
          return false;
      }

      // Check for infinitive linking loops
      if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
           return false;
      }

      // Quick checking of one object being a subset of another.
      // todo: cache the structure of arguments[0] for performance
      for (p in y) {
          if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
              return false;
          }
          else if (typeof y[p] !== typeof x[p]) {
              return false;
          }
      }

      for (p in x) {
          if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
              return false;
          }
          else if (typeof y[p] !== typeof x[p]) {
              return false;
          }

          switch (typeof (x[p])) {
              case 'object':
              case 'function':

                  leftChain.push(x);
                  rightChain.push(y);

                  if (!compare2Objects (x[p], y[p])) {
                      return false;
                  }

                  leftChain.pop();
                  rightChain.pop();
                  break;

              default:
                  if (x[p] !== y[p]) {
                      return false;
                  }
                  break;
          }
      }

      return true;
    }

    if (arguments.length < 1) {
      return true; //Die silently? Don't know how to handle such case, please help...
      // throw "Need two or more arguments to compare";
    }

    for (i = 1, l = arguments.length; i < l; i++) {

        leftChain = []; //Todo: this can be cached
        rightChain = [];

        if (!compare2Objects(arguments[0], arguments[i])) {
            return false;
        }
    }

    return true;
  }
}


export class EmailManager {
  private userId: string;
  private deleteDayFilter: GoogleAppsScript.Gmail.GmailLabel;
  private deleteWeekFilter: GoogleAppsScript.Gmail.GmailLabel;
  private deleteHourFilter: GoogleAppsScript.Gmail.GmailLabel;
  private deleteMonthFilter: GoogleAppsScript.Gmail.GmailLabel;
  private deleteYearFilter: GoogleAppsScript.Gmail.GmailLabel;
  private deleteDecadeFilter: GoogleAppsScript.Gmail.GmailLabel;
  private conversationHistoryLabel: GoogleAppsScript.Gmail.GmailLabel;

  constructor() {
    this.userId = "gscanlon21@gmail.com" // The alias is "me";
    this.conversationHistoryLabel = GmailApp.getUserLabelByName("Conversation History");
    this.deleteHourFilter = GmailApp.getUserLabelByName("Delete Hour");
    this.deleteDayFilter = GmailApp.getUserLabelByName("Delete Day");
    this.deleteWeekFilter = GmailApp.getUserLabelByName("Delete Week");
    this.deleteMonthFilter = GmailApp.getUserLabelByName("Delete Month");
    this.deleteYearFilter = GmailApp.getUserLabelByName("Delete Year");
    this.deleteDecadeFilter = GmailApp.getUserLabelByName("Delete Decade");
  }

  private deleteAfterDate(threads: GoogleAppsScript.Gmail.GmailThread[], millisecondOffset: number) {
    let currentDate = Date.now();

    for (let i = 0; i < threads.length; i++) {
      let thread: GmailThread = threads[i];

      let defaultWeight = 1;
      let keepsakeArray = [
        thread.isImportant() ? 4 : defaultWeight,
        thread.isUnread()    ? 2 : defaultWeight,
        thread.isInTrash()   ? 2 : defaultWeight // Keep for an additional millisecondOffset when trashed, so it doesn't get deleted at the next execution
      ]
      // Increase offset per importance weights
      let keepsakeWeight = keepsakeArray.reduce((partialTotal, a) => partialTotal * a);

      // Stop double checking, this is right
      // timeSinceLastMessageDate increases as the message gets older
      let timeSinceLastMessageDate = currentDate - thread.getLastMessageDate().getTime(); 
      let weightedMaxKeepTime = millisecondOffset * keepsakeWeight;
      let isRecentMessage = timeSinceLastMessageDate < weightedMaxKeepTime;
      if (isRecentMessage || thread.hasStarredMessages()) {
        continue;
      }

      if (thread.isInInbox()) {
        thread.moveToTrash();
      }
      else if (thread.isInTrash() || thread.isInSpam()) {
        Gmail.Users().Threads().remove(this.userId, thread.getId());
      }
    }
  }

  public wrapper() {
    this.generalWrapper();
    this.deleteWrapper();
  }

  public generalWrapper() {
    this.checkMarkConversations();
    this.manageFilters();
  }

  public deleteWrapper() {
    this.deleteHour();
    this.deleteDay();
    this.deleteWeek();
    this.deleteMonth();
    this.deleteYear();
    this.deleteDecade();
  }

  private manageFilters() {
    let filters = Gmail.Users.Settings.Filters.list(this.userId);

    for (let i = 0; i < filters.length; i++) {
      let filter1 = filters[i];
      for (let j = i + 1; j < filters.length; j++) {
        let filter2 = filters[j];

        if (filter1.action.Helprs.deepCompare(filter2.action)) {
          let newQuery = "(" + (filter1.query.replace(/^\(+/,'').replace(/\)+$/,'') + " OR " + filter2.query.replace(/^\(+/,'').replace(/\)+$/,'')) + ")";
          let newFrom = "(" +  (filter1.from.replace(/^\(+/,'').replace(/\)+$/,'') + " OR " + filter2.from.replace(/^\(+/,'').replace(/\)+$/,'')) + ")";
          let newSubject = "(" +  (filter1.subject.replace(/^\(+/,'').replace(/\)+$/,'') + ")  subject:(" + filter2.subject.replace(/^\(+/,'').replace(/\)+$/,'')) + ")";

          let newFilter = { criteria: { query: newQuery, from: newFrom, subject: newSubject }, action: filter1.action };
          Gmail.Users.Settings.Filters.create(newFilter, this.userId);

          filter1.remove();
          filter2.remove();
        }
      }
    }
  }

  private checkMarkConversations() {
    let threads = GmailApp.getInboxThreads();

    for (let i = 0; i < threads.length; i++) {
      let thread = threads[i];

      if (thread.getMessageCount() > 3) {
        thread.addLabel(this.conversationHistoryLabel);
      }
    }
  }

  private deleteHour() {
    let threads = this.deleteHourFilter.getThreads();
    this.deleteAfterDate(threads, 3600000);
  }

  private deleteDay() {
    let threads = this.deleteDayFilter.getThreads();
    this.deleteAfterDate(threads, 86400000);
  }

  private deleteWeek() {
    let threads = this.deleteWeekFilter.getThreads();
    this.deleteAfterDate(threads, 604800000);
  }

  private deleteMonth() {
    let threads = this.deleteMonthFilter.getThreads();
    this.deleteAfterDate(threads, 2678400000);
  }

  private deleteYear() {
    let threads = this.deleteYearFilter.getThreads();
    this.deleteAfterDate(threads, 31536000000);
  }

  private deleteDecade() {
    let threads = this.deleteDecadeFilter.getThreads();
    this.deleteAfterDate(threads, 315360000000);
  }
}

function deleteWrapper() {
  var emailManager = new EmailManager();
  emailManager.deleteWrapper();
}

function wrapper() {
  var emailManager = new EmailManager();
  emailManager.wrapper();
}

function generalWrapper() {
  var emailManager = new EmailManager();
  emailManager.generalWrapper();
}
