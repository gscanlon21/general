#! usr/bin/python3
# -*- coding: utf-8 -*-
if __name__ == '__main__':
    import unittest


'''
A non-empty zero-indexed array A consisting of N integers is given.
Array A represents a linked list. A list is constructed from this array as follows:

- The first node (the head) is located at index 0
- The value of a node located at index K is A[K]
- If the value of a node is −1 then it is the last node of the list
- Otherwise, the successor of a node located at index K is located at index
... A[K] (you can assume that A[K] is a valid index, that is 0 ≤ A[K] < N)

For example, for array A such that:
  A[0] =  1
  A[1] =  4
  A[2] = -1
  A[3] =  3
  A[4] =  2

The following list is constructed:
- The first node (the head) is located at index 0 and has a value of 1
- The second node is located at index 1 and has a value of 4
- The third node is located at index 4 and has a value of 2
- The fourth node is located at index 2 and has a value of −1

Write a function that, given a non-empty zero-indexed array A consisting of
N integers, returns the length of the list constructed from A in the above manner.

For example, given array A such that:
  A[0] =  1
  A[1] =  4
  A[2] = -1
  A[3] =  3
  A[4] =  2
the function should return 4

Assume that:
- N is an integer within the range [1..200,000]
- Each element of array A is an integer within the range [−1..N−1]
- It will always be possible to construct the list and its length will be finite.
'''


class Solution:
    def __init__(self):
        pass

    def length(self, array):
        newArray = [array[0]]
        while -1 not in newArray:
            index = newArray[-1]
            newArray.append(array[index])

        return len(newArray)


class MyTest(unittest.TestCase):
    def test_me(self):
        S = Solution()
        aE = self.assertEqual
        aE(S.length([1, 4, -1, 3, 2]), 4)  # OK
        aE(S.length([-1]), 1)  # OK
        aE(S.length([2, -1, 1]), 3)  # OK
        aE(S.length([1, -1, 2, 2, 3, 4, 5, 6, -1]), 2)  # OK


if __name__ == '__main__':
    unittest.main()
