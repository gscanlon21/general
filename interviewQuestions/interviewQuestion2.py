#!usr/bin/python3
# -*- coding: utf-8 -*-

if __name__ == '__main__':
    import unittest


'''
/**
*
* Given a back to back requested meetings in minutes,
* and we have to take a break after each meeting,
* we'd like to accept/deny certain proposal
* so that we can have a maximum amount of meeting duration.
*
* Proposed input:
*
* [10, 20, 30] => pick [10, 30]
* [10, 50, 30] => pick [50]
* [20, 10, 30, 50] => pick [20, 50]
*
**/
'''

# TODO
#
# Comment
# Debug
# More unittests


class Meetings:
    def __init__(self):
        pass

    def maxDuration(self, array=[]):
        # Checks for valid input
        if not isinstance(array, list):
            return ("# maxDuration(array:array) # <class 'TypeError'> # Enter a valid array. Returned.")
        elif len(array) < 1:
            return ("# maxDuration(array:array) # <class 'ValueError'> # Enter a valid array. Returned.")

        diction = {}
        copyArray = array[:]
        returnArray = []
        while max(array) > 0:
            arrayMax = max(array)
            indexMax = array.index(arrayMax)
            indexPrev = indexMax - 1
            indexNext = indexMax + 1
            try:
                totalAround = array[indexNext] + array[indexPrev]
            except Exception:
                totalAround = 0
            if totalAround == 0:
                try:
                    while (array[-1] == 0 and array[1] == 0 and len(array) > 2):
                        array.pop(-1)
                except Exception:
                    pass
                if indexMax + 1 == len(array):
                    returnArray.append(arrayMax)
                    diction.setdefault(arrayMax, []).append(indexMax)
                    array[indexMax] = 0
                    array[indexPrev] = 0
                elif indexMax == 0:
                    returnArray.append(arrayMax)
                    diction.setdefault(arrayMax, []).append(indexMax)
                    array[indexMax] = 0
                    array[indexNext] = 0
                else:
                    array[indexMax] = 0
            elif totalAround > arrayMax:
                slicedArray = array[:indexPrev] + array[indexNext + 1:]
                if len(slicedArray) > 1:
                    returnArray.append(arrayMax)
                    diction.setdefault(arrayMax, []).append(indexMax)
                    array.pop(indexNext)
                    array.pop(indexPrev)
                else:
                    returnArray.append(array[indexPrev])
                    returnArray.append(array[indexNext])
                    diction.setdefault(array[indexPrev], []).append(
                        indexPrev * (len(copyArray) - len(returnArray)))
                    diction.setdefault(array[indexNext], []).append(
                        indexNext * (len(copyArray) - len(returnArray)))
                    array[indexPrev] = 0
                    array[indexNext] = 0
            else:
                returnArray.append(arrayMax)
                diction.setdefault(arrayMax, []).append(indexMax)
                array[indexMax] = 0
                array[indexPrev] = 0
                array[indexNext] = 0

        sortedReturnArray = [-1] * len(copyArray)
        for key, value in diction.items():
            for num in value:
                sortedReturnArray[num] = key
        sortedReturnArray = list(filter((-1).__ne__, sortedReturnArray))
        return sortedReturnArray


class MyTest(unittest.TestCase):
    def test_me(self):
        M = Meetings()
        aE = self.assertEqual
        # Given tests
        aE(M.maxDuration([10, 20, 30]), [10, 30])  # OK
        aE(M.maxDuration([10, 50, 30]), [50])  # OK
        aE(M.maxDuration([20, 10, 30, 50]), [20, 50])  # OK
        aE(M.maxDuration([30, 50, 30]), [30, 30])  # OK
        aE(M.maxDuration([20, 10, 30, 50]), [20, 50])  # OK
        aE(M.maxDuration([100, 20, 3, 100, 20, 50, 40]), [100, 100, 50])  # OK
        aE(M.maxDuration([11, 500, 30, 11, 50, 30, 10, 50, 300]), [500, 50, 10, 300])  # OK
        aE(M.maxDuration([40, 30, 50, 30, 40]), [40, 50, 40])  # OK
        aE(M.maxDuration([20, 10, 30, 50, 20, 10, 30, 50]), [20, 50, 10, 50])  # OK
        aE(M.maxDuration([20, 10, 30, 50, 30, 10, 30, 50]), [20, 50, 10, 50])  # NOT OK
        # Custom tests
        aE(M.maxDuration(
            []), "# maxDuration(array:array) # <class 'ValueError'> # Enter a valid array. Returned.")  # OK
        aE(M.maxDuration(
        ), "# maxDuration(array:array) # <class 'ValueError'> # Enter a valid array. Returned.")  # OK
        aE(M.maxDuration(
            True), "# maxDuration(array:array) # <class 'TypeError'> # Enter a valid array. Returned.")  # OK
        aE(M.maxDuration(
            'String'), "# maxDuration(array:array) # <class 'TypeError'> # Enter a valid array. Returned.")  # OK


if __name__ == '__main__':
    # Meetings().maxDuration([20, 10, 30, 50, 20, 10, 30, 50]) # Debugger
    unittest.main()  # unittests
