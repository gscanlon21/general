#! python3
# -*- coding: utf-8 -*-
import unittest

# Should return the max money to be made from buying stocks
# In left to right order
# Buy low and sell high
# Each element in an array corresponds to that stocks daily high


class Stocks:
    def __init__(self):
        pass

    def maxMoney(self, array):
        money = 0
        firstindex = 0
        previousindex = 0

        if len(array) == 0:
            return 0

        for index, item in enumerate(array[1:]):
            if item < array[previousindex]:
                money += array[previousindex] - array[firstindex]
                firstindex = index + 1
            previousindex += 1

        money += array[previousindex] - array[firstindex]

        return money


class MyTest(unittest.TestCase):
    def test_me(self):
        s = Stocks()
        self.assertEqual(s.maxMoney([25, 30, 29, 31]), (7))  # OK
        self.assertEqual(s.maxMoney([100, 90, 80, 70]), (0))  # OK
        self.assertEqual(s.maxMoney([10, 30, 50, 70]), (60))  # OK
        self.assertEqual(s.maxMoney([100, 90, 110, 95, 105]), (30))  # OK
        self.assertEqual(s.maxMoney([100, 90, 80, 90, 100]), (20))  # OK
        self.assertEqual(s.maxMoney([100, 110, 120, 110, 100]), (20))  # OK
        self.assertEqual(s.maxMoney([25, 31, 29, 30]), (7))  # OK
        self.assertEqual(s.maxMoney([]), (0))  # OK
        self.assertEqual(s.maxMoney([1, 2]), (1))  # OK
        self.assertEqual(s.maxMoney([2, 1]), (0))  # OK
        self.assertEqual(s.maxMoney([1]), (0))  # OK


unittest.main()
