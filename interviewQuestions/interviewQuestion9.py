#! usr/bin/python3
# -*- coding: utf-8 -*-
if __name__ == '__main__':
    import unittest


'''
You are given an implementation of a function that, given an integer M and a
zero-indexed array A consisting of N non-negative integers, which are not
greater than M, returns the value (or one of the values) that occurs most
often in this array.

For example, given M = 3 and array A such that:
  A[0] = 1
  A[1] = 2
  A[2] = 3
  A[3] = 3
  A[4] = 1
  A[5] = 3
  A[6] = 1
the function may return 1 or 3.

Assume that:
N is an integer within the range [1..200,000];
M is an integer within the range [1..10,000];
each element of array A is an integer within the range [0..M].

Complexity:
- expected worst-case time complexity is O(N+M);
- expected worst-case space complexity is O(M), beyond input storage (not
... counting the storage required for input arguments).

Elements of input arrays can be modified.
'''


class Solution:
    def __init__(self):
        pass

    def mostAppearancesInList(self, listOfNumbers):
        count = [listOfNumbers.count(num) for num in listOfNumbers]
        return (listOfNumbers[count.index(max(count))])


class MyTest(unittest.TestCase):
    def test_me(self):
        S = Solution()
        aE = self.assertEqual
        aE(S.mostAppearancesInList([99, 98, 97, 99, 99, 98, 97]), 99)  # OK
        aE(S.mostAppearancesInList([1, 2, 3, 2, 1]), 1)  # OK
        aE(S.mostAppearancesInList([1, 1, 1, 2, 2, 2, 1, 2, 1, 2, 2]), 2)  # OK


if __name__ == '__main__':
    unittest.main()
