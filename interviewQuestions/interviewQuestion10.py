#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Write a function that

import unittest
from random import randint
from timeit import repeat

# The approximate question was:
#
# Knowing a dictionary rulesDict:
# ... rulesDict = {'AB': 'AA', 'BA': 'AA', 'CB': 'CC',
# ...              'BC': 'CC', 'AA': 'A', 'CC': 'C'}
# And given a string consisting of only A, B or C:
# As long as a dictionary key remains in stringABC
# ... Choose a key value pair at random
# ... Replace that key with the coresponding value only once for each iteration
# Best time complexity should be O(N)


class Solution:
    def __init__(self):
        pass

    def solution(self, stringABC):
        rulesDict = {'AB': 'AA', 'BA': 'AA', 'CB': 'CC', 'BC': 'CC', 'AA': 'A', 'CC': 'C'}
        rulesList = ['AB', 'BA', 'CB', 'BC', 'AA', 'CC']

        # Complexity O(N**2)
        while any([key for key in rulesDict.keys() if key in stringABC]):
            # randint() includes the ending number
            rule = rulesList[randint(0, 5)]
            ruleChange = rulesDict.get(rule)
            stringABC = stringABC.replace(rule, ruleChange, 1)

        return stringABC

    def solution2(self, stringABC):
        rulesDict = {'AB': 'AA', 'BA': 'AA', 'CB': 'CC', 'BC': 'CC', 'AA': 'A', 'CC': 'C'}
        rulesList = ['AB', 'BA', 'CB', 'BC', 'AA', 'CC']

        sameString = 0

        # O(N) complexity
        # This will be faster than solution() if I can
        # ... figure out the optimal place to stop the loop
        # Might fail because of the randomness
        while sameString < 30:
            # randint() includes the ending number
            rule = rulesList[randint(0, 5)]
            ruleChange = rulesDict.get(rule)
            oldString = stringABC
            stringABC = stringABC.replace(rule, ruleChange, 1)
            if oldString == stringABC:
                # Needs to increment for a while to make
                # ... sure the all the rules are checked
                # ... before completing the loop
                sameString += 1
            else:
                sameString = 0

        return stringABC


class MyTest(unittest.TestCase):
    def testMe(self):
        S = Solution()
        aE = self.assertEqual
        aE(S.solution('ABC'), 'AC')  # OK
        aE(S.solution('ABBA'), 'A')  # OK
        aE(S.solution('CBA'), 'CA')  # OK
        aE(S.solution('CCC'), 'C')  # OK
        aE(S.solution('BBB'), 'BBB')  # OK
        aE(S.solution('ABBACAA'), 'ACA')  # OK

        aE(S.solution2('ABC'), 'AC')  # OK
        aE(S.solution2('ABBA'), 'A')  # OK
        aE(S.solution2('CBA'), 'CA')  # OK
        aE(S.solution2('CCC'), 'C')  # OK
        aE(S.solution2('BBB'), 'BBB')  # OK
        aE(S.solution2('ABBACAA'), 'ACA')  # OK


if __name__ == '__main__':
    print(min(repeat('Solution().solution("ABBACAA")', number=1000, globals=globals(), repeat=10)))
    # 0.05277034300524974

    print(min(repeat('Solution().solution2("ABBACAA")', number=1000, globals=globals(), repeat=10)))
    # 0.08217500599857885

    unittest.main()
