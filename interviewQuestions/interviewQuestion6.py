#! usr/bin/python3
# -*- coding: utf-8 -*-
import unittest
from timeit import repeat

'''
Write a fucntion that, given a non-empty zero-indexed array A consisting
of N integers representing the coins, returns the maximum possible adjacency
that can be obtained by reversing exactly one coin (that is, one of the coins
must be reversed). Consecutive elements of array A represent consecutive coins
in the row. Array A contains only 0s and/or 1s:
0 represents a coin with heads facing up;
1 represents a coin with tails facing up.

For example, given array A consisting of six numbers, such that:
  A[0] = 1
  A[1] = 1
  A[2] = 0
  A[3] = 1
  A[4] = 0
  A[5] = 0
the function returns 4. The initial adjacency is 2, as there are two pairs
of adjacent coins showing the same face, namely (0, 1) and (4, 5). After
reversing the coin represented by A[2] the adjacency equals 4, as there are
four pairs of adjacent coins showing the same face, namely (0, 1), (1, 2),
(2, 3) and (4, 5), and it is not possible to obtain a higher adjacency.
'''


class Coins:
    def __init__(self):
        pass

    def adjacent(self, flipList):
        # My original solution
        if len(flipList) == 2 and flipList.count(0) % 2 == 0:
            return 1

        if flipList.count(1) >= flipList.count(0):
            num = 0
        else:
            num = 1

        while flipList[-1:] == [num] and flipList.count(num) > 1:
            flipList.pop()
        while flipList[:1] == [num] and flipList.count(num) > 1:
            del(flipList[:1])

        while flipList[-1:] == flipList[:1] and flipList[1:2] == flipList[-2:-1] and len(flipList) > 3 and flipList.count(num) >= 2:
            flipList = flipList[1:-1]

        while flipList.count(num) > 1 and flipList.count(num) < flipList.count(abs(num - 1)):
            del(flipList[flipList.index(num)])
            flipList.pop()

        return len(flipList)

    def adjacent2(self, flipList):
        # The bugged solution
        n = len(flipList)
        result = 0
        for i in range(n - 1):
            if (flipList[i] == flipList[i + 1]) or result == 0:  # ADDED ` or result == 0`
                result = result + 1
        r = 0
        for i in range(n):
            count = 0
            if (i > 0):
                if (flipList[i - 1] != flipList[i]):
                    count = count + 1
                else:
                    count = count - 1
            if (i < n - 1):
                if (flipList[i + 1] != flipList[i]):
                    count = count + 1
                else:
                    pass  # REMOVED: `count = count - 1`
            r = max(r, count)

        return (result + r) if n != 1 else 1  # REMOVED: `result + r`


class MyTest(unittest.TestCase):
    def test_me(self):
        C = Coins()
        aE = self.assertEqual
        # My original solution
        aE(C.adjacent([1, 1, 0, 1, 0, 0]), 4)  # OK
        aE(C.adjacent([1, 0, 1, 0, 1, 0]), 3)  # OK
        aE(C.adjacent([0, 1, 0, 1, 0, 1, 0, 1]), 3)  # OK
        aE(C.adjacent([1, 1, 1, 0, 1, 1, 1]), 7)  # OK
        aE(C.adjacent([0, 1, 1, 0]), 3)  # OK
        aE(C.adjacent([0, 1, 1, 1, 1, 1, 0]), 6)  # OK
        aE(C.adjacent([]), 0)  # OK
        aE(C.adjacent([1]), 1)  # OK
        aE(C.adjacent([1, 1, 1, 1, 0, 0, 1]), 5)  # OK
        aE(C.adjacent([1, 0, 0, 1, 1, 1, 1]), 5)  # OK
        aE(C.adjacent([1, 1, 1, 1, 0, 0]), 5)  # OK
        aE(C.adjacent([0, 0, 1, 1, 1, 1]), 5)  # OK
        aE(C.adjacent([0, 1, 1, 1, 1, 0, 0]), 5)  # OK
        aE(C.adjacent([0, 0, 1, 1, 1, 1, 0]), 5)  # OK
        # One Coin Must be Flipped
        aE(C.adjacent([0, 0]), 1)  # OK
        aE(C.adjacent([1, 1]), 1)  # OK

        # The bugged solution
        aE(C.adjacent2([1, 1, 0, 1, 0, 0]), 4)  # OK
        aE(C.adjacent2([1, 0, 1, 0, 1, 0]), 3)  # OK
        aE(C.adjacent2([0, 0]), 1)  # OK
        aE(C.adjacent2([0, 1, 0, 1, 0, 1, 0, 1]), 3)  # OK
        aE(C.adjacent2([0, 1, 1, 0]), 3)  # OK
        aE(C.adjacent2([0, 1, 1, 1, 1, 1, 0]), 6)  # OK
        aE(C.adjacent2([]), 0)  # OK
        aE(C.adjacent2([1]), 1)  # OK
        aE(C.adjacent2([1, 1, 1, 1, 0, 0, 1]), 5)  # OK
        aE(C.adjacent2([1, 1, 1, 1, 0, 0]), 5)  # OK
        aE(C.adjacent2([0, 0, 1, 1, 1, 1]), 5)  # OK
        aE(C.adjacent2([0, 0, 1, 1, 1, 1, 0]), 5)  # OK
        # aE(C.adjacent2([0, 1, 1, 1, 1, 0, 0]), 5)  # NOT OK
        # aE(C.adjacent2([1, 0, 0, 1, 1, 1, 1]), 5)  # NOT OK
        # aE(C.adjacent2([1, 1, 1, 0, 1, 1, 1]), 7)  # NOT OK


if __name__ == '__main__':
    print(min(repeat('Coins().adjacent([1, 1, 0, 1, 0, 0])', number=1000000, globals=globals(), repeat=10)))
    # 2.331441137001093

    print(min(repeat('Coins().adjacent2([1, 1, 0, 1, 0, 0])', number=1000000, globals=globals(), repeat=10)))
    # 3.868193040998449

    unittest.main()
