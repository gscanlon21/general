#! usr/bin/python3
# -*- coding: utf-8 -*-
import unittest

'''
The Fibonacci sequence is defined using the following recursive formula:
 F(0) = 0
 F(1) = 1
 F(N) = F(N−1) + F(N−2)    if N ≥ 2

Write a function that, given a non-negative integer N, returns the six least
significant decimal digits of number F(N).

For example, given N = 8, the function should return 21, because the six least
significant decimal digits of F(8) are 000021 (the complete decimal
representation of F(8) is 21). Similarly, given N = 36, the function should
return 930352, because the six least significant decimal digits of F(36)
are 930352 (the complete decimal representation of F(36) is 14930352).

Assume that:
N is an integer within the range [0..2,147,483,647].

Complexity:
expected worst-case time complexity is O(log(N));
expected worst-case space complexity is O(log(N)).
'''


class Fib:
    def __init__(self):
        pass

    def leastSignificantDigitsOfFib(self, number):
        fib = str(self.fib(number))
        return int(fib[-6:])

    def fib(self, number):
        number = int(number)
        if number <= 1:
            return number
        return self.fib(number - 1) + self.fib(number - 2)


class MyTest(unittest.TestCase):
    def test_me(self):
        F = Fib()
        aE = self.assertEqual
        aE(F.leastSignificantDigitsOfFib(8), 21)  # OK
        aE(F.leastSignificantDigitsOfFib(36), 930352)  # OK


if __name__ == '__main__':
    unittest.main()
